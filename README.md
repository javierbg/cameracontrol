Este programa permite controlar las cámaras instaladas por la Universidad de Córdoba desde sistemas Linux.

### Requisitos

En sistemas basados en Debian (Debian, Ubuntu, Linux Mint, ...) instalar los siguientes paquetes:

```
sudo apt install uvcdynctrl libxcb-xinerama0
```

* `uvcdynctrl` es la herramienta utilizada para controlar la cámara
* `libxcb-xinerama0` es necesario para los bindings de Qt


En sistemas basados en Arch (Arch, Manjaro...) instalar `libwebcam-git` (provee el comando `uvcdynctrl`) desde AUR.

Una vez instalados estos requisitos puede ejecutarse el binario disponible en [la página de releases](https://gitlab.com/javierbg/cameracontrol/-/releases).

Opcionalmente, si el binario no funcionara, el software está diseñado para funcionar en Python >= 3.5. Para lanzarlo se necesita tener instalado el módulo PySide2, ya sea desde `pip` en un entorno virtual o a nivel del sistema. 

Se incluye un fichero _spec_ para construir un fichero ejecutable que incluye el intérprete y los ficheros y módulos necesarios para ejecutar el software. Este es el mismo que se ha utilizado para generar el binario.

Los presets se guardan en el fichero "presets.json". Se pueden editar a mano o
se puede gestionar todo desde la interfaz gráfica (menos eliminar presets).
El formato del fichero JSON es el siguiente:
- En la raíz se indica el aula.
- Cada aula tiene una serie de presets
- Cada preset debe indicar los valores de pan, tilt y zoom para mover la cámara

Si se prefiere, se puede utilizar el ejecutable como herramienta de la línea de
comandos:

```
./ccontrol <aula> <preset>
```

Esto moverá inmediatamente la cámara al preset seleccionado.