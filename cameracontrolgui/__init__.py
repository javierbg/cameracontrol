# This Python file uses the following encoding: utf-8
import sys
import os


from PySide2.QtWidgets import QApplication, QWidget, QAbstractItemView, QStyleOptionSlider, QInputDialog, QErrorMessage
from PySide2.QtGui import QStandardItem, QStandardItemModel
from PySide2.QtCore import QFile
from PySide2.QtUiTools import QUiLoader


# Define function to import external files when using PyInstaller.
def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


class CameraControlGUI(QWidget):
    def __init__(self, presets, control_limits, control_function):
        super(CameraControlGUI, self).__init__()
        self.presets = presets
        self.control_limits = control_limits
        self.control_function = control_function
        self.load_ui()

    def load_ui(self):
        loader = QUiLoader()
        path = resource_path("form.ui")
        ui_file = QFile(path)
        ui_file.open(QFile.ReadOnly)
        self.ui = loader.load(ui_file, self)
        for slider in self.get_sliders().values():
            slider.sliderReleased.connect(self.move_camera)

        self.ui.nuevaAulaButton.clicked.connect(self.add_new_classroom)
        self.ui.nuevoPresetButton.clicked.connect(self.add_new_preset)
        self.ui.guardarConfiguracionButton.clicked.connect(self.save_preset)
        self.ui.moverCamaraButton.clicked.connect(self.move_camera)
        self.set_slider_limits()
        self.populate_preset_treeview()

    def get_sliders(self):
        return {
            'pan': self.ui.panSlider,
            'tilt': self.ui.tiltSlider,
            'zoom': self.ui.zoomSlider,
        }

    def set_slider_limits(self):
        sliders = self.get_sliders()

        for name, slider in sliders.items():
            slider.setMinimum(self.control_limits[name][0])
            slider.setMaximum(self.control_limits[name][1])
            slider.setTickInterval(self.control_limits[name][2])

    def set_slider_values(self, preset):
        sliders = self.get_sliders()
        for name, slider in sliders.items():
            slider.setValue(preset[name])

    def get_slider_values(self):
        sliders = self.get_sliders()
        return {name: slider.value() for name, slider in sliders.items()}

    def populate_preset_treeview(self):
        view = self.ui.presetsTreeView
        view.setSelectionBehavior(QAbstractItemView.SelectRows)
        model = QStandardItemModel()
        view.setModel(model)
        view.setUniformRowHeights(True)
        selectionModel = view.selectionModel()
        selectionModel.selectionChanged.connect(self.on_selection_changed)
        selectionModel.selectionChanged.connect(self.move_camera)

        for aula, ps in self.presets.items():
            aulaEntry = QStandardItem(aula)
            for p in ps.keys():
                presetEntry = QStandardItem(p)
                aulaEntry.appendRow(presetEntry)
            model.appendRow(aulaEntry)

    def on_selection_changed(self, selected, deselected):
        index = selected.indexes()[0]
        model = self.ui.presetsTreeView.model()
        item = model.itemFromIndex(index)

        if item.parent() is None:
            return

        ps = item.text()
        aula = item.parent().text()
        self.set_slider_values(self.presets[aula][ps])

    def add_new_classroom(self):
        text, ok = QInputDialog.getText(self, 'Nueva aula', 'Introduce el nombre de la nueva aula:')

        if (not ok) or (not text):
            return

        model = self.ui.presetsTreeView.model()
        for aula_index in range(model.rowCount()):
            if model.item(aula_index).text() == text:
                return

        self.presets[text] = dict()
        model.appendRow(QStandardItem(text))

    def add_new_preset(self):
        idxs = self.ui.presetsTreeView.selectionModel().selectedIndexes()
        if len(idxs) == 0:
            return
        model = self.ui.presetsTreeView.model()
        item = model.itemFromIndex(idxs[0])

        if item.parent() is not None:
            item = item.parent()
        aula = item.text()

        text, ok = QInputDialog.getText(self, 'Nuevo preset', 'Introduce el nombre del nuevo preset:')

        if not ok:
            return

        i = 0
        c = item.child(i)
        while c is not None:
            if c.text() == text:
                return
            i += 1
            c = item.child(i)

        self.presets[aula][text] = dict(pan=0, tilt=0, zoom=0)
        item.setChild(i, QStandardItem(text))

    def save_preset(self):
        idxs = self.ui.presetsTreeView.selectionModel().selectedIndexes()
        if len(idxs) == 0:
            return
        model = self.ui.presetsTreeView.model()
        item = model.itemFromIndex(idxs[0])

        if item.parent() is None:
            return

        aula = item.parent().text()
        preset = item.text()

        slider_values = self.get_slider_values()
        self.presets[aula][preset] = slider_values

    def move_camera(self):
        slider_values = self.get_slider_values()
        self.control_function(slider_values)


if __name__ == "__main__":
    app = QApplication([])
    widget = CameraControlGUI()
    widget.show()
    sys.exit(app.exec_())
