#!/usr/bin/env python3

import json
import re
import subprocess
import sys
from pathlib import Path
from typing import Union, Dict, Tuple

from PySide2.QtWidgets import QApplication, QSizePolicy

from cameracontrolgui import CameraControlGUI


attribute_names = {
    'pan': 'Pan (Absolute)',
    'tilt': 'Tilt (Absolute)',
    'zoom': 'Zoom, Absolute'
}


def camera_is_controllable(camera_path: Path) -> Union[None, Dict]:
    cp = subprocess.run(['uvcdynctrl', '-cv', '-d', str(camera_path)],
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    cpout = cp.stdout.decode('utf8')

    if any(n not in cpout for n in attribute_names.values()):
        return None

    rexp = r'{}\s*\n\s*ID[^\n]*\n\s*Type[^\n]*\n\s*Flags[^\n]*\n\s*Values\s*:\s*' \
           r'\[\s*(?P<min>-?\d+)\s*\.\.\s*(?P<max>-?\d+)\s*,\s*step size:\s*(?P<step>-?\d+)\s*\],'
    matches = {att: re.search(rexp.format(re.escape(name)), cpout, re.DOTALL)
               for att, name in attribute_names.items()}

    return {att: (int(m.group('min')), int(m.group('max')), int(m.group('step')))
            for att, m in matches.items()}


def load_presets(path='presets.json'):
    path = Path(path)
    if not path.exists():
        with path.open('w') as f_out:
            json.dump(dict(), f_out)
    elif not path.is_file():
        raise RuntimeError('"{}" es un directorio'.format(str(path)))

    with path.open('r') as f_in:
        presets = json.load(f_in)
    return presets


def save_presets(presets, path='presets.json'):
    with open(path, 'w') as f_out:
        json.dump(presets, f_out, indent=4)


def get_available_camera() -> Tuple[Path, Dict]:
    available_cameras = ((p, camera_is_controllable(p)) for p in Path('/dev').glob('video*'))
    available_cameras = list(filter(lambda c: c[1] is not None, available_cameras))

    if not available_cameras:
        raise RuntimeError("Cámara no detectada")

    if len(available_cameras) > 1:
        print("Se detectó más de una cámara, activando una de ellas", file=sys.stderr)

    return available_cameras[0]


def send_control_command(active_camera, attribute, value):
    return subprocess.run(['uvcdynctrl', '-d', str(active_camera), '-s', attribute,
                           '--', str(value)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

def apply_preset(preset, active_camera: Path):
    # print('Moviendo cámara "{}" a la siguiente posicion: {}'.format(active_camera, preset))

    rets = (send_control_command(active_camera, name, preset[att]) for att, name in attribute_names.items())

    if any(ret.returncode != 0 for ret in rets):
        raise RuntimeError('Error moviendo la cámara')


if __name__ == '__main__':
    active_camera, control_limits = get_available_camera()
    # active_camera, control_limits = None, dict(pan=(-2,2,1),tilt=(-2,2,1),zoom=(-2,2,1))

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('aula', type=str, nargs='?')
    parser.add_argument('preset', type=str, nargs='?')
    args = parser.parse_args()

    if (args.aula is None) != (args.preset is None):
        parser.error('Ejecuta el programa sin argumentos para una GUI, o con ambos para mover la cámara')

    presets = load_presets()

    if (args.aula is not None) and (args.preset is not None):
        if args.aula not in presets.keys():
            raise ValueError('El aula "{}" no se encuentra en los presets'.format(args.aula))
        if args.preset not in presets[args.aula].keys():
            raise ValueError('El preset "{}" no se encuentra en los presets del aula'.format(args.preset))
        preset = presets[args.aula][args.preset]

        apply_preset(preset, active_camera)

    else:
        app = QApplication([])
        widget = CameraControlGUI(presets, control_limits, lambda p: apply_preset(p, active_camera))
        widget.window().setWindowTitle("Control de la cámara")
        widget.window().setFixedSize(670, 380)
        widget.show()
        exit_code = app.exec_()
        save_presets(presets)
        sys.exit(exit_code)
